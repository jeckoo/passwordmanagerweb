﻿function DownloadFile(filename, bytesBase64) {
    var link = document.createElement('a');
    link.download = filename;
    link.href = "data:application/octet-stream;charset=utf-8;base64," + bytesBase64;
    document.body.appendChild(link); // Needed for Firefox
    link.click();
    document.body.removeChild(link);
}

function LoadFile() {
    return new Promise((resolve, reject) => {
        var input, file, fr;
        input = document.getElementById('fileinput');
        file = input.files[0];
        fr = new FileReader()
        fr.onload = (e) => resolve(fr.result);
        fr.onerror = (e) => reject(fr.error);
        fr.readAsBinaryString(file);
    })
    
}

function ReadPassword() {
    return new Promise((resolve, reject) => {
        input = document.getElementById('psw');
        resolve(input.value);
    })
    
}

function CopyToClipboard(text) {
    return navigator.clipboard.writeText(text);
}